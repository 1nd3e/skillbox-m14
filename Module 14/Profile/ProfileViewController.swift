//
//  ProfileViewController.swift
//  Module 14
//
//  Created by Vladislav Kulikov on 01.03.2020.
//  Copyright © 2020 Vladislav Kulikov. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    
    // MARK: - Private Properties
    
    private var name: String? {
        get { return Defaults.shared.name }
        set { Defaults.shared.name = newValue }
    }
    
    private var surname: String? {
        get { return Defaults.shared.surname }
        set { Defaults.shared.surname = newValue }
    }
    
    // MARK: - Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let name = name, let surname = surname {
            nameLabel.text = name
            surnameLabel.text = surname
        }
    }
    
}

// MARK: - IBAction

extension ProfileViewController {
    
    @IBAction func changeName(_ nameTextField: UITextField) {
        if nameTextField.text == "" { return }

        name = nameTextField.text
        nameLabel.text = name
    }
    
    @IBAction func changeSurname(_ surnameTextField: UITextField) {
        if surnameTextField.text == "" { return }

        surname = surnameTextField.text
        surnameLabel.text = surname
    }
    
    @IBAction func textFieldShouldReturn(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
}
