//
//  CurrentWeather.swift
//  Module 12
//
//  Created by Vladislav Kulikov on 26.02.2020.
//  Copyright © 2020 Vladislav Kulikov. All rights reserved.
//

import Foundation

class CurrentWeather {
    
    // MARK: - Public Properties
    
    let temperature: Int
    
    // MARK: - Initializers
    
    init?(data: NSDictionary) {
        guard let temperature = data["temp"] as? NSNumber else { return nil }
        self.temperature = temperature.intValue
    }
    
}
