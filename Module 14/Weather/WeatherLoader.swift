//
//  WeatherLoader.swift
//  Module 12
//
//  Created by Vladislav Kulikov on 26.02.2020.
//  Copyright © 2020 Vladislav Kulikov. All rights reserved.
//

import Foundation

class WeatherLoader {
    
    func loadCurrentWeather(completion: @escaping (CurrentWeather) -> Void) {
        let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?q=Moscow&units=metric&APPID=f07c22616d041534096b3cbdb8752a2d")!
        let request = URLRequest(url: url)
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            if let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
                let jsonDict = json as? NSDictionary {
                for (_, data) in jsonDict where data is NSDictionary {
                    if let currentWeather = CurrentWeather(data: data as! NSDictionary) {
                        DispatchQueue.main.async {
                            completion(currentWeather)
                        }
                    }
                }
            }
        })
        
        task.resume()
    }
    
    func loadForecastWeather(completion: @escaping ([ForecastWeather]) -> Void) {
        let url = URL(string: "https://api.openweathermap.org/data/2.5/forecast?q=Moscow&units=metric&APPID=f07c22616d041534096b3cbdb8752a2d")!
        let request = URLRequest(url: url)
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            if let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
                let jsonDict = json as? NSDictionary,
                let forecastList = jsonDict["list"] as? NSArray {
                var forecastWeathers: [ForecastWeather] = []
                
                for data in forecastList {
                    if let forecastWeather = ForecastWeather(data: data as! NSDictionary) {
                        forecastWeathers.append(forecastWeather)
                    }
                }
                
                DispatchQueue.main.async {
                    completion(forecastWeathers)
                }
            }
        })
        
        task.resume()
    }
    
}
