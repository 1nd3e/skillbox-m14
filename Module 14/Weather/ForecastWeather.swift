//
//  ForecastWeather.swift
//  Module 12
//
//  Created by Vladislav Kulikov on 26.02.2020.
//  Copyright © 2020 Vladislav Kulikov. All rights reserved.
//

import Foundation

class ForecastWeather: NSObject, NSCoding {
    
    // MARK: - Public Properties
    
    let temperature: Int
    
    var date: String {
        let date = Date(timeIntervalSince1970: dateTime)
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.dateStyle = .medium
        
        return dateFormatter.string(from: date)
    }
    
    // MARK: - Private Properties
    
    private let dateTime: Double
    
    // MARK: - Initializers
    
    init?(data: NSDictionary) {
        guard let weatherData = data["main"] as? NSDictionary,
            let temperature = weatherData["temp"] as? NSNumber,
            let dateTime = data["dt"] as? NSNumber else {
                return nil
        }
        
        self.temperature = temperature.intValue
        self.dateTime = dateTime.doubleValue
    }
    
    required init?(coder: NSCoder) {
        self.temperature = coder.decodeInteger(forKey: "temperature") 
        self.dateTime = coder.decodeDouble(forKey: "dateTime")
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(temperature, forKey: "temperature")
        coder.encode(dateTime, forKey: "dateTime")
    }
    
}
