//
//  WeatherViewController.swift
//  Module 12
//
//  Created by Vladislav Kulikov on 24.02.2020.
//  Copyright © 2020 Vladislav Kulikov. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    @IBOutlet weak var weatherTableView: UITableView!
    
    // MARK: - Private Properties
    
    private var currentTemperature: Int {
        get { return Defaults.shared.currentTemperature }
        set { Defaults.shared.currentTemperature = newValue }
    }
    
    private var forecastWeathers: [ForecastWeather] {
        get { return Defaults.shared.forecastWeathers }
        set { Defaults.shared.forecastWeathers = newValue }
    }
    
    // MARK: - Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadCurrentWeather()
        loadForecastWeather()
    }

}

// MARK: - Private Methods

extension WeatherViewController {
    
    private func loadCurrentWeather() {
        WeatherLoader().loadCurrentWeather { currentWeather in
            self.currentTemperature = currentWeather.temperature
            self.currentTemperatureLabel.text = "\(self.currentTemperature)°"
        }
        
        currentTemperatureLabel.text = "\(currentTemperature)°"
    }
    
    private func loadForecastWeather() {
        WeatherLoader().loadForecastWeather { forecastWeathers in
            self.forecastWeathers = forecastWeathers
            self.weatherTableView.reloadData()
        }
    }
    
}

// MARK: - UITableViewDataSource

extension WeatherViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        forecastWeathers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! WeatherTableViewCell
        cell.temperatureLabel.text = "\(forecastWeathers[indexPath.row].temperature)°"
        cell.dateLabel.text = forecastWeathers[indexPath.row].date
        
        return cell
    }
    
}
