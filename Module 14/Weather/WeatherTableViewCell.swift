//
//  WeatherTableViewCell.swift
//  Module 12
//
//  Created by Vladislav Kulikov on 25.02.2020.
//  Copyright © 2020 Vladislav Kulikov. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {
    
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

}
