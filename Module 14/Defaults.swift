//
//  Defaults.swift
//  Module 14
//
//  Created by Vladislav Kulikov on 01.03.2020.
//  Copyright © 2020 Vladislav Kulikov. All rights reserved.
//

import Foundation

class Defaults {
    
    // MARK: - Types
    
    static let shared = Defaults()
    
    // MARK: - Constants
    
    private let nameKey = "nameKey"
    private let surnameKey = "surnameKey"
    
    private let currentTemperatureKey = "currentTemperatureKey"
    private let forecastWeathersKey = "forecastWeathersKey"
    
    // MARK: - Public Properties
    
    var name: String? {
        get { return UserDefaults.standard.string(forKey: nameKey) }
        set { UserDefaults.standard.set(newValue, forKey: nameKey) }
    }
    
    var surname: String? {
        get { return UserDefaults.standard.string(forKey: surnameKey) }
        set { UserDefaults.standard.set(newValue, forKey: surnameKey) }
    }
    
    var currentTemperature: Int {
        get { return UserDefaults.standard.integer(forKey: currentTemperatureKey) }
        set { UserDefaults.standard.set(newValue,  forKey: currentTemperatureKey) }
    }
    
    var forecastWeathers: [ForecastWeather] {
        get {
            var decodedData: [ForecastWeather] = []
            
            if let data = UserDefaults.standard.data(forKey: forecastWeathersKey) {
                do {
                    decodedData = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as! [ForecastWeather]
                } catch {}
            }
            
            return decodedData
        }
        
        set {
            do {
                let encodedData = try NSKeyedArchiver.archivedData(withRootObject: newValue, requiringSecureCoding: false)
                UserDefaults.standard.set(encodedData, forKey: forecastWeathersKey)
            } catch {}
        }
    }
    
}
