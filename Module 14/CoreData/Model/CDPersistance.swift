//
//  CDPersistance.swift
//  Module 14
//
//  Created by Vladislav Kulikov on 02.03.2020.
//  Copyright © 2020 Vladislav Kulikov. All rights reserved.
//

import UIKit
import CoreData

class CDPersistance {
    
    // MARK: - Types
    
    static let shared = CDPersistance()
    
    // MARK: - Public Properties
    
    var tasks: [CDTask] {
        get {
            var taskList: [CDTask] = []
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            let fetchRequest: NSFetchRequest<CDTask> = CDTask.fetchRequest()
            
            do {
                taskList = try context.fetch(fetchRequest)
            } catch {
                print(error.localizedDescription)
            }
            
            return taskList
        }
        set {}
    }
    
    // MARK: - Public Methods
    
    func addTask(withTitle title: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "CDTask", in: context)
        let taskObject = NSManagedObject(entity: entity!, insertInto: context) as! CDTask
        taskObject.title = title
        
        do {
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func deleteTask(withIndex i: Int) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        context.delete(tasks[i])
        
        do {
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
}
