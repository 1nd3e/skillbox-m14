//
//  CDToDoViewController.swift
//  Module 14
//
//  Created by Vladislav Kulikov on 02.03.2020.
//  Copyright © 2020 Vladislav Kulikov. All rights reserved.
//

import UIKit
import CoreData

class CDToDoViewController: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Lifecycle Methods

    override func viewDidLoad() {
        super.viewDidLoad()
    }

}

// MARK: - IBAction

extension CDToDoViewController {
    
    @IBAction func addTask(_ button: UIBarButtonItem) {
        let alert = UIAlertController(title: "Новая задача", message: nil, preferredStyle: .alert)
        
        alert.addTextField { textField in
            textField.placeholder = "Купить хлеб"
            
            textField.autocorrectionType = .yes
            textField.autocapitalizationType = .sentences
        }
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Добавить", style: .default, handler: { _ in
            if let inputText = alert.textFields?.first?.text {
                if inputText == "" { return }
                
                CDPersistance.shared.addTask(withTitle: inputText)
                self.tableView.reloadData()
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

// MARK: - UITableViewDataSource

extension CDToDoViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let tasks = CDPersistance.shared.tasks
        
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tasks = CDPersistance.shared.tasks
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! CDTaskTableViewCell
        cell.titleLabel.text = tasks[indexPath.row].title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        
        CDPersistance.shared.deleteTask(withIndex: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    
}
