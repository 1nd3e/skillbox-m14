//
//  TaskTableViewCell.swift
//  Module 14
//
//  Created by Vladislav Kulikov on 02.03.2020.
//  Copyright © 2020 Vladislav Kulikov. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!

}
