//
//  Persistance.swift
//  Module 14
//
//  Created by Vladislav Kulikov on 02.03.2020.
//  Copyright © 2020 Vladislav Kulikov. All rights reserved.
//

import Foundation
import RealmSwift

class Task: Object {
    
    // MARK: - Public Properties
    
    @objc dynamic var title = String()
    
}

class Persistance {
    
    // MARK: - Types
    
    static let shared = Persistance()
    
    // MARK: - Constants
    
    private let realm = try! Realm()
    
    // MARK: - Public Properties
    
    var tasks: Results<Task> {
        return realm.objects(Task.self)
    }
    
    // MARK: - Public Methods
    
    func addTask(withTitle title: String) {
        let task = Task()
        task.title = title
        
        try! realm.write {
            realm.add(task)
        }
    }
    
    func deleteTask(withIndex i: Int) {
        let task = tasks[i]
        
        try! realm.write {
            realm.delete(task)
        }
    }
    
}
